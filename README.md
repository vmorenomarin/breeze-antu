Breeze Transparent with Antü icons: A transparent and clear theme for Plasma 5, inspired by Breeze Transparent and the icons of Antü Plasma themes.

This fork is based on the work of David Edmundson adn Fabián Alexis.

For installation, uncompress the file and copy the folder into /home/user/.local/share/plasma/desktoptheme/

where user is your user name.

Comments and observations: vmorenomarin@gmail.com 
